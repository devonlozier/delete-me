
// Demo Two
// Devon Lozier

#include <iostream> // cout/cin
#include <conio.h> // _getch()

using namespace std;

//DEFINITION
// Method - A function that is inside of a class

int main() // Not a method -- just a function
{
	// conditionals (if/switch)
	if (0) return 0; // anything non-zero is 'true'

	//switch / switch-case
	int i;

	cout << "Enter a day of Christmas (1-12): ";
	cin >> i;


	switch (i)
	{
	case 12: cout << "Twelve Drummers Drumming\n"; break; // case fall-through removes break. Shows up until the line that has a break.
	case 11: cout << "Eleven Pipers Piping\n"; break;
	case 10: cout << "Ten Lords a Leaping\n"; break;
	case 9: cout << "Nine Ladies Dancing\n"; break;
	case 8: cout << "Eight Maids a Milking\n"; break;
	case 7: cout << "Seven Swans a Swimming\n"; break;
	case 6: cout << "Six Geese a Laying\n"; break;
	case 5: cout << "Five Golden Rings\n"; break;
	case 4: cout << "Four Calling Birds\n"; break;
	case 3: cout << "Three French Hens\n"; break;
	case 2: cout << "Two Turtle Doves\n"; break;
	case 1: cout << "A Partridge in a Pear Tree\n"; break;
	default: cout << i << " is not a valid day of Christmas.\n";
	}


	// looping
	// for -- you know the numbers!
	for ( int i = 0; i < 7; i++)
	{
		cout << "--";
		
	}

	// while / do-while
	bool ryanIsCool = true;
	while (ryanIsCool)
	{
		bool ryanLies = true;
		if (ryanLies) ryanIsCool = false;
	}

	(void)_getch();
	return 0;

}